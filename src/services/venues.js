define([
  'jquery'
], function($) {
  var URL = 'https://api.foursquare.com/v2/venues/explore?';

  function getVenues(params, successCb, errorCb) {
    var fetchParams = [
      'oauth_token=QXG1MC3GD0ZS4VETREZMEQBB3OV01OFYGAENJZ5L2WFZ541X',
      'v=20170704'
    ];

    fetchParams.push('ll=' + params.lat + ',' + params.lng);

    if (typeof params.radius === 'number') {
      fetchParams.push('radius=' + params.radius);
    }

    $.get(URL + fetchParams.join('&'), successCb).fail(errorCb);
  }

  return {
    getVenues: getVenues
  }
});
