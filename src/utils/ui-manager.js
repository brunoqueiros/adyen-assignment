define([
  'jquery',
  'text!templates/alert.html',
  'text!templates/box.html',
  'text!templates/empty-result.html'
], function($, alertHTML, boxHTML, emptyResultHTML) {
  function updateNotification(text, type) {
    if ($('#app-notification').length === 0) {
      $('#app-content-root').append($(alertHTML));
    }

    if (type !== '') {
      $('#app-notification .icon').addClass('is-hidden');
    }

    $('#app-notification .notification-text').text(text);
    $('#app-notification')
      .addClass('is-' + type)
      .removeClass('is-hidden');
  }

  function hideNotification() {
    $('#app-notification').addClass('is-hidden');
  }

  function updateVenues(venues) {
    var output = [];
    var newVenue;

    if (venues.length === 0) {
      output.push($(emptyResultHTML));
    } else {
      $.each(venues, function (index, venueItem) {
        newVenue = $(boxHTML);
        newVenue.find('.app-venue-name').text(venueItem.venue.name);
        newVenue.find('.app-venue-location').text(venueItem.venue.location.address);
        newVenue.find('.app-venue-phone')
          .attr('href', 'tel:' + venueItem.venue.contact.phone)
          .find('.text')
            .text(venueItem.venue.contact.phone);
        newVenue.find('.app-venue-facebook')
          .attr('href', 'https://facebook.com/' + venueItem.venue.contact.facebook);
        newVenue.find('.app-venue-twitter')
          .attr('href', 'https://twitter.com/' + venueItem.venue.contact.twitter);
        newVenue.find('.app-venue-type').text(venueItem.venue.categories[0].name);

        if (venueItem.venue.hasOwnProperty('price')) {
          newVenue.find('.app-venue-price').text(venueItem.venue.price.message + ' - ');
        }

        if (venueItem.venue.ratingColor) {
          newVenue.find('.app-venue-rating')
            .css('background-color', '#' + venueItem.venue.ratingColor)
            .css('color', '#FFFFFF')
            .text(venueItem.venue.rating);
        }

        if (venueItem.hasOwnProperty('tips')) {
          newVenue.find('.app-venue-tip').text(venueItem.tips[0].text);
        }

        output.push(newVenue);
      });
    }

    $('#app-venue-list').empty().append(output);
    $('#app-venue-container').removeClass('is-hidden');
  }

  function cloneVenueElement() {
    return $('#app-venue-box').clone().removeClass('is-hidden').removeAttr('id');
  }

  function updateText(element, text) {
    $(element).text(text);
  }

  function updateValue(element, value) {
    $(element).val(value);
  }

  function hideAppVenueContainer() {
    $('#app-venue-container').addClass('is-hidden');
  }

  return {
    updateVenues: updateVenues,
    hideNotification: hideNotification,
    updateNotification: updateNotification,
    updateValue: updateValue,
    hideAppVenueContainer: hideAppVenueContainer
  }
});
