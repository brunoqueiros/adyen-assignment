define([
  'app-state'
], function(AppState) {
  var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  /**
   * Get user current location,
   * return AppState.state.userLocation in case
   * the browser already triggered getCurrentPosition
   */
  function getLocation(successCb, errorCb) {
    if (!AppState.state.userLocation) {
      navigator.geolocation.getCurrentPosition(function(location) {
        successCb(location);
      }, errorCb, options);
    } else {
      successCb(AppState.state.userLocation);
    }
  }

  return {
    getLocation: getLocation
  }
});
