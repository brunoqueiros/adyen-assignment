requirejs.config({
  baseUrl: 'src/',
  paths: {
    jquery: '../bower_components/jquery/dist/jquery.min',
    text: '../bower_components/text/text'
  }
});

requirejs([
  'app-state',
  'utils/get-user-location',
  'services/venues',
  'utils/ui-manager'
], function(AppState, UserLocation, VenuesService, UIManager) {
  /**
   * Fetch venues next to current location
   */
  function fetchVenues() {
    AppState.updateState({
      alertMessage: 'Fetching venues near to you'
    });

    VenuesService.getVenues({
      lat: AppState.state.userLocation.coords.latitude,
      lng: AppState.state.userLocation.coords.longitude,
      radius: AppState.state.radius || null
    }, onVenuesSuccess, function(error) {
      AppState.updateState({
        alertMessage: 'Sorry, can\'t get venues near to you!',
        isFetching: false,
        alertType: 'danger'
      });
    });
  }

  /**
   * Update AppState with the venues
   */
  function onVenuesSuccess(res) {
    var newState = {
      venues: res.response.groups[0].items,
      isFetching: false
    };

    if (typeof res.response.suggestedRadius === 'number') {
      newState['radius'] = res.response.suggestedRadius;
    }

    AppState.updateState(newState);
  }

  /**
   * Get user current location
   */
  function getCurrentLocation() {
    AppState.updateState({
      isFetching: true,
      alertMessage: 'Getting your current location'
    });

    UserLocation.getLocation(function(location) {
      AppState.updateState({ userLocation: location });
    }, function(error) {
      AppState.updateState({
        alertMessage: 'Sorry, can\'t get your location!',
        isFetching: false,
        alertType: 'danger'
      });
    });
  }

  /**
   * Handle all app state changes,
   * triggering UI changes
   */
  function handleStateChange(event, changes) {
    if (changes.indexOf('radius') > -1) {
      if (!AppState.state.isFetching && changes.length === 1) {
        getCurrentLocation();
      }

      UIManager.updateValue('#app-venue-radius', AppState.state.radius);
    }

    if (changes.indexOf('userLocation') > -1) {
      fetchVenues();
    }

    if (changes.indexOf('venues') > -1) {
      UIManager.hideNotification();
      UIManager.updateVenues(AppState.state.venues);
    }

    if (changes.indexOf('alertMessage') > -1) {
      UIManager.hideAppVenueContainer();
      UIManager.updateNotification(AppState.state.alertMessage, AppState.state.alertType);
    }
  }

  $(document).on('state-has-update', handleStateChange);

  $('#app-radius-button').on('click', function () {
    AppState.updateState({
      radius: parseInt($('#app-venue-radius').val(), 10)
    });
  });

  getCurrentLocation();
});
