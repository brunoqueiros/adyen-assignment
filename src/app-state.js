define(['jquery'], function ($) {
  var state = {
    isFetching: false,
    alertType: '',
    alertMessage: '',
    userLocation: null,
    radius: null,
    venues: []
  };

  function updateState(newState) {
    state = Object.assign(state, newState);
    $(document).trigger('state-has-update', [ Object.keys(newState) ]);
  }

  return {
    state: state,
    updateState: updateState
  };
});
