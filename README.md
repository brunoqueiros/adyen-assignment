# Venues finder

## Dependencies
- jQuery
- RequireJS
- Bulma

## Code architecture
As Tim explained to me that Adyen doesn't use framework for security reasons, I decided to create the application using vanilla javascript + jQuery + RequireJS (to keep the code modular), all code were write using ES5 as described at the assignment description.

The application data flow uses the same idea from React by creating an application state `src/app-state.js`, this file is where all application data are stored. To change the application state you should call `AppState.updateState` function and pass the new state, when the state is updated is trigged an event `state-has-update` where `src/app.js` has function `handleStateChange` to manage all changes.


***src/templates***
HTML partials files

**src/services/venues.js**
Service to fetch the Foursquare API.

**src/utils/ui-manager.js**
Manages all DOM changes.

**src/utils/get-user-location.js**
Get the user's current location, using Geolocation API.

**src/app-state.js**
Where all application data are stored, and also have the function `updateState` responsible to update the state.

**src/app.js**
Main entry point, and where all iterations are (trigger geolocation, fetch api)



```
- bower_components (dependencies folder)
- node_modules (server dependencies folder)
- src
	- templates
	- services
	- utils
- index.html
```


## How to run
```
npm start
```
Access http://localhost:3000/