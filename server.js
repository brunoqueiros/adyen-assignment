var StaticServer = require('static-server');
var server = new StaticServer({
  rootPath: '.',
  port: 3000,
  cors: '*',
  templates: {
    index: 'index.html'
  }
});
 
server.start(function () {
  console.log('Server listening to', server.port);
});